# Simple ECS Example

## What is this example?

An example of a basic game written in JavaScript and using the ECS model of structuring logic and data.

You are the green square, you eat the blue squares and avoid the red.

This isn't a full featured game. As the repository name implies it is a 'Simple ECS Example'.

## What is ECS?

In short ECS is a way to structure your code that is more data oriented and less reliant on large inheritance hierarchies. This project is an extremely simple implementation of ECS in JavaScript and is intended to be used to help beginners understand ECS.

Checkout the [Wikipedia](https://en.wikipedia.org/wiki/Entity_component_system), on ECS for more information.

## How to run.

Just clone the repository, and open the `index.html` file in a modern browser.
