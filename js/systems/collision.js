// A immediately invoked function expression is used to keep the collision
// function from leaking to the global scope: e.g. "rectRectCollision",
// "playerCollisionLogic".
(function() {
  game.system.collision = function(entity1, entity2) {
    if(entity1 === undefined || entity2 === undefined) return;
    // Always be sure to check that you are not checking the same entity against
    // itself when dealing with a double for loop.
    if(entity1 === entity2) return;
    if(!entity1.collides || !entity2.collides) return;
    if(!entity1.position || !entity2.position) return;
    if(!entity1.size     || !entity2.size) return;

    if(entity1.type === 'player')
      playerCollisionLogic(entity1, entity2);

  };

  // Breaking collision apart into functions like this makes it extremely clear
  // as to what is taking place. You get fine grain control over names of
  // entities inside of these functions: e.g. "player", "enemy".
  var playerCollisionLogic = function(player, enemy) {
    if(!rectRectCollision(player, enemy)) return;

    if(player.size.w >= enemy.size.w) {
      player.size.w += 2;
      player.size.h += 2;

      enemy.dead = true;
    } else {
      player.dead = true;
    }
  };

  // Generic rectangle against rectangle collision, checker
  var rectRectCollision = function(entity1, entity2) {
    if(
      entity1.position.x + entity1.size.w > entity2.position.x &&
      entity1.position.y + entity1.size.h > entity2.position.y &&
      entity2.position.x + entity2.size.w > entity1.position.x &&
      entity2.position.y + entity2.size.h > entity1.position.y
    ) return true;

    return false;
  };
}());
