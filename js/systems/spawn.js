// Wrapped in an immediately invoked function expression to prevent leaking
// "playerWidth" variable and "spawnLogic" function to the global scope.
(function() {
  var playerWidth;

  game.system.spawn = function(entity) {
    if(entity === undefined) return;

    // Saving player size data
    if(entity.type === 'player')
      playerWidth = entity.size.w;

    if(entity.type === 'spawner')
      spawnLogic(entity);
  };

  function spawnLogic(entity) {
    var randomValue = Math.floor(Math.random() * 40);
    if(randomValue !== 1) return;

    // Spawn component allows for different spawner entities to create
    // different entities, this one is just creating the generic "enemy" entity.
    if(entity.spawn === 'enemy') {
      if(playerWidth === undefined) {
        game.entities.push(new game.entityTemplate.enemy);
      } else {
        game.entities.push(new game.entityTemplate.enemy(playerWidth));
      }
    }
  }
}());
