// Wrapped in an immediately invoked function expression to prevent leaking size
// variables to the global scope.
(function() {
  var color1 = '#F00', // An enemy larger than the player gets this color
      color2 = '#00F', // An enemy smaller than the player gets this color
      color3 = '#0F0'; // A fallback color for player

  var playerWidth;

  game.system.render = function(entity) {
    if(entity === undefined) return;
    if(!entity.position || !entity.size) return;

    // Saving player size data
    if(entity.type === 'player')
      playerWidth = entity.size.w;

    if(entity.type === 'enemy') {
      if(playerWidth === undefined) {
        entity.color = color1;
      } else {
        if(entity.size.w > playerWidth) entity.color = color1;
        else if(entity.size.w <= playerWidth) entity.color = color2;
      }
    }

    // Drawing squares
    game.ctx.fillStyle = entity.color;
    game.ctx.fillRect(
      entity.position.x,
      entity.position.y,
      entity.size.w,
      entity.size.h
    );
  };
}());
