game.system.movement = function(entity) {
  // These intial 3 if statements check for basic truths about an entity. If an
  // entity is undefined; doesn't have a "moves", "position" or "size" component
  // set to true; then we don't run movement logic.
  if(entity === undefined) return;
  if(!entity.moves) return;
  if(!entity.position || !entity.size) return;

  if(entity.type === 'player') {
    // If mouse hasn't been moved, return
    if(!game.mouse.x && !game.mouse.y) return;

    entity.position.x = game.mouse.x - entity.size.w/2;
    entity.position.y = game.mouse.y - entity.size.h/2;
  }

  if(entity.type === 'enemy') {
    if(!entity.velocity) return;

    entity.position.x += entity.velocity.dx;
    entity.position.y += entity.velocity.dy;

    // Kill enemy entity when it moves outside of room
    if(
      entity.position.x > game.canvas.width ||
      entity.position.x + entity.size.w < 0
    ) entity.dead = true;
  }
};
