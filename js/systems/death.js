// Having death be it's own system is a pesonal design choice, but it could be
// baked into the collision system.
game.system.death = function(entity, i) {
  if(entity === undefined) return;
  if(!entity.dead) return;

  if(entity.type === 'player') {
    game.entities.splice(i, 1);
    alert('You died, refresh page.');
  }

  if(entity.type === 'enemy') {
    game.entities.splice(i, 1);
  }
};
