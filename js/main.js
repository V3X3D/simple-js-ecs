"use strict";

// Global Scope Variable
window.game = {};

// Canvas and Context
game.canvas = document.getElementById('game-canvas');
game.ctx = game.canvas.getContext('2d', { alpha: false });
game.ctx.webkitImageSmoothingEnabled = false;
game.ctx.imageSmoothingEnabled = false;

// Global Game Variables
game.canvas.width  = 640;
game.canvas.height = 480;
// ECS
game.entities = [];
game.component = {};
game.system = {};

// This object is used to store a template of an entity. This allows for
// making copies if needed. Example, the enemies in this game are constantly
// created and pushed to 'game.entities' array in the file 'systems/spawn.js'.
game.entityTemplate = {};

game.update = function() {
  // Update calls
  for(var i=0; i<game.entities.length; i++) {
    // Some systems require a check between two entities, we handle these with
    // a secondary for loop, and 2 passable system parameters -- one for each
    // entity.
    for(var z=0; z<game.entities.length; z++) {
      game.system.collision(game.entities[i], game.entities[z]);
    }

    game.system.movement(game.entities[i]);
    game.system.spawn(game.entities[i]);
    game.system.death(game.entities[i], i);
  }
};

game.render = function() {
  // Render calls
  game.ctx.fillStyle = '#099';
  game.ctx.fillRect(0, 0, game.canvas.width, game.canvas.height);

  // Loop through render systems
  for(var i=0; i<game.entities.length; i++) {
    game.system.render(game.entities[i]);
  }
};

// Begin Game Loop
window.onload = function loop() {
  game.update();
  game.render();
  window.requestAnimationFrame(loop);
};
