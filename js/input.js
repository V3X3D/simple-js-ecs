game.mouse = {
  x: 0,
  y: 0
};

game.canvas.addEventListener('mousemove', function(e) {
  var bounds = game.canvas.getBoundingClientRect();

  game.mouse.x = e.clientX - bounds.left;
  game.mouse.y = e.clientY - bounds.top;
});
