game.component.position = function(x, y) {
  this.x = x || 0;
  this.y = y || 0;

  return this;
};

game.component.size = function(w, h) {
  this.w = w || 0;
  this.h = h || 0;

  return this;
};

game.component.velocity = function(dx, dy) {
  this.dx = dx || 0;
  this.dy = dy || 0;

  return this;
};
