// Base entity constructor function, with add and remove functions for adding
// and removing components. 
game.entityConstructor = function() {

  this.addComponent = function(name, component) {
    this[name] = component;
  };
  this.removeComponent = function(name) {
    delete this[name];
  };

  return this;
};

