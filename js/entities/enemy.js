game.entityTemplate.enemy = function(baseWidth) {
  baseWidth = baseWidth || 32;

  var enemy = new game.entityConstructor(),
      w = Math.floor(Math.random() * 14) + baseWidth-5,
      h = w,
      // x set to random value on left or right side of screen.
      x = (Math.floor(Math.random() * 2) * game.canvas.width) - w/2,
      y = Math.floor(Math.random() * game.canvas.height) - h/2,
      dx = undefined, // set a few lines down
      dy = 0;         // set to 0, enemies only move left or right

  if(x < game.canvas.width/2) dx = 2;
  else dx = -2;

  enemy.addComponent('type', 'enemy');
  enemy.addComponent('dead', false);
  enemy.addComponent('moves', true);
  enemy.addComponent('collides', true);
  enemy.addComponent('position', new game.component.position(x, y));
  enemy.addComponent('size', new game.component.size(w, h));
  enemy.addComponent('velocity', new game.component.velocity(dx, dy));

  return enemy;
};
