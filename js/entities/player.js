// Adding player to our "entityTemplate" object, allows us to create new
// instances as needed. A good example of this is the enemy "entityTemplate"
// being called in the "systems/spawn.js" file.
game.entityTemplate.player = function() {
  var player = new game.entityConstructor();

  player.addComponent('type', 'player');
  player.addComponent('dead', false);
  player.addComponent('moves', true);
  player.addComponent('collides', true);
  player.addComponent(
    'position',
    new game.component.position(game.canvas.width/2, game.canvas.height/2)
  );
  player.addComponent('size', new game.component.size(32, 32));
  player.addComponent('color', '#0F0');

  return player;
};
// Push a new instance of player to our "entities" array. Could also push a new
// instance if the player has died and needs to respawn, etc.
game.entities.push(new game.entityTemplate.player);
