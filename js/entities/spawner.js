game.entityTemplate.enemySpawner = function() {
  var spawner = new game.entityConstructor();

  spawner.addComponent('type', 'spawner');
  spawner.addComponent('spawn', 'enemy');

  return spawner;
};
game.entities.push(new game.entityTemplate.enemySpawner);
